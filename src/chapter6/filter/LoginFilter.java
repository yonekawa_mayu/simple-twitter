package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;

@WebFilter(urlPatterns={"/setting/*", "/edit/*"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;

		User user = (User)(req.getSession().getAttribute("loginUser"));

		if(user != null) {
	        chain.doFilter(req, res);// サーブレットを実行
		}else {
			List<String> errorMessages = new ArrayList<String>();
            errorMessages.add("ログインをしてください");
            req.setAttribute("errorMessages", errorMessages);
            req.getRequestDispatcher("login.jsp").forward(req, res);
            res.sendRedirect("login.jsp");
		}
	}

	@Override
	public void init(FilterConfig fConfig) {
	
	}
	
	@Override
	public void destroy() {
	
	}

}